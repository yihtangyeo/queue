/*
University of Toronto Faculty of Applied Science and Engineering
Division of Engineering Science
CSC192 Computer Programming and Data Structures

Title: Assignment 4
Question 2 (a): queueArray.c

Student name: YEO, Yih Tang
*/

#include <stdio.h>
#include <stdlib.h>

// define the size of queue to be 5
#define NUM 5

typedef int Item;                   // define new data type Item that is int
typedef enum {false, true} bool;    // define new type of boolean that can have value true and false

// FUNCTION PROTOTYPES - DO NOT EDIT //
void make_empty(void);
bool is_empty(void);
bool is_full(void);
void enqueue (Item i);
Item dequeue(void);
Item first(void);
Item last(void);
//     END OF FUNCTION PROTOTYPE     //

// create a struct type that uses to be a LINKED LIST, by having a num, and a pointer pointing to the next element
typedef struct node* queue;
struct node
{
    Item num;
    queue next;
};

// create three pointers pointing to struct node: one is the first pointer pointing to the first node in the queue,
// one is the last pointer pointing to the last ndde, and the intermediate pointer is used when storing new node in the queue (will explain in function definition)
queue firstPointer = NULL, lastPointer = NULL, intermediatePointer = NULL;

int main()
{
    int user_selection; // variable store the user selection based on the MENU displayed below
    int temporaryInt;   // variable temprarily stores the number that the user wants to put into the queue when enqueue is called

    printf("size of queue is declared to %d\n", NUM);

    do
    {
        // display the menu and prompt users to make a selection
        printf("Choose an action:\n1: make_empty()\n2: is_empty\n3: is_full\n4: enqueue\n5: dequeue\n6: first\n7: last\n8: QUIT\n");
        printf("Please choose: ");
        scanf("%d", &user_selection);

        // if user makes a selection beyond acceptable range 1-8, print error and show the menu again.
        if (user_selection < 1 || user_selection > 9){
            printf("Error: invalid selection.\n");
            continue;
        }

        switch(user_selection)
        {
            case 1:                         // make the list empty
                make_empty();
                break;
            case 2:                         // check if the list is empty
                printf("is_empty: ");
                if (is_empty())
                    printf("true\n");
                else
                    printf("false\n");
                break;
            case 3:                         // check if the list is full
                printf("is_full: ");
                if (is_full())
                    printf("true\n");
                else
                    printf("false\n");
                break;
            case 4:                         // enqueue an item to the END, prompt user to enter the value
                printf("Enter item to enqueue: ");
                scanf("%d", &temporaryInt);
                enqueue(temporaryInt);
                break;
            case 5:                         // dequeue an item from the BEGINNING
                printf("Item that is being dequeued: %d\n", dequeue());
                break;
            case 6:                         // access the first element in the queue
                printf("first: %d\n", first());
                break;
            case 7:                         // access the last element in the queue
                printf("last: %d\n", last());
                break;
        }

        printf("\n");

    } while (user_selection != 8);
    // repeat showing the menu until the user chooses to quit.

    return 0;
}

void make_empty(void){

    // start making the queue empty by clearing its beginning value first (to be consistent with FIFO)
    while(firstPointer!=NULL){

        // use a temporary pointer to point to the next node in the queue after firstPointer
        queue temporaryPointer = firstPointer->next;
        // clear the current point and free the memory
        free(firstPointer);
        // the first pointer is now holding the next node in the queue
        firstPointer = temporaryPointer;

    } // repetitively clear the node until there is no more left, when firstPointer is NULL

    printf("Successful: Array is now empty.\n", firstPointer);
}

bool is_empty(void){
    // to check if the queue is empty, check if the firstPointer is pointing to nothing
    if (firstPointer == NULL){
        return true;
    }
    else{
        return false;
    }
}

bool is_full(void){

    // if the queue is empty, it is impossible for it to be full
    if (is_empty() == true){
        return false;
    }

    // if the queue is NOT empty, it must have a node in the queue, so set the counter to one, sa the checker in the initialization of the for loop points to the firstPointer
    int counter = 1;

    // create a temporary pointer that will be used to check each node in the queue
    queue checker;
    // for loop: set the pointer to the first, repeat until the last and when the pointer is pointing to nothing; or else, keep reading the next node by pointing to the "next" of the current node
    for (checker = firstPointer; checker!=lastPointer && checker!=NULL; checker = checker->next){
        counter++;  // increment the counter too
    }
    // if the counter reaches the maximum number of the queue, return true as it is full
    if (counter == NUM){
        return true;
    }
    else{
        return false;
    }
}

void enqueue(Item i){

    /*
    HOW ENQUEUE WORKS?
    1. Check if empty - if empty, use firstPointer to call for dynamic memory allocation, and set all pointers pointing to the same node
    2. If NOT empty, use the lastPointer to call for dynamic memory allocation
    3. When a new node is added, the intermediatePointer will still point to the old node, while lastPointer gets a new value
    4. After lastPointer is defined, the intermediatePointer that is pointing to the previous node, will now know where to point to for a new node, and store it into the "next" of previous node which it is pointing to
    5. After that, (since intermediatePointer has finished its job of combining the previous node to the new node), it will point to the same node as the lastPointer, so that when another new node is added, it can do the job again
    6. At this point, intermediatePointer now acts similar to lastPointer, pointing to the last element.
    7. Therefore, we can store the value into intermediatePointer->num, and then set intermediatePointer->next to be NULL, since the last node has no where to point to
    A diagram (simple) can look like this:

    (Last in, Last Out) lastPointer --> [  num | next = NULL ] <--- [ num | next = TO THE LEFT ] <--- [ num | next = TO THE LEFT ] <--- firstPointer (First In, First Out)

    */

    // if it is full, impossible to enqueue
    if (is_full() == true){
        printf("Fail to enqueue: Queue is full\n");
    }
    else{

        if (is_empty() == true){                                            // [1]
            firstPointer = malloc(sizeof(struct node));

            if (firstPointer == NULL){
                printf("Error: Dynamic Memory Allocation failed.\n");
                exit(EXIT_FAILURE);
            }
            lastPointer = intermediatePointer = firstPointer;               // [1]

        }
        else{                                                               // [2]
            lastPointer = malloc(sizeof(struct node));
            if (lastPointer == NULL){
                printf("Error: Dynamic Memory Allocation failed.\n");
                exit(EXIT_FAILURE);
            }
            intermediatePointer->next = lastPointer;                        // [3][4]
            intermediatePointer = lastPointer;                              // [5][6]

        }

        intermediatePointer->next = NULL;                                   // [7]
        intermediatePointer->num = i;                                       // [7]
        printf("Enqueue successful: number %d is stored.\n", i);


    }
}

Item dequeue(void){
    // before dequeuing, check if there is something to be dequeued, which is, is the queue empty
    if (is_empty() == true){
        printf("Error: unable to dequeue, queue is empty\n");
        return 0;
    }
    Item i = firstPointer->num;                         // store the value pointed by firstPointer
    queue temporaryPointer = firstPointer->next;        // store the next "NEW" firstPointer that will take effect after firstPointer is deleted
    free(firstPointer);                                 // free the firstPointer as it is not used anymore
    firstPointer = temporaryPointer;                    // the firstPointer is set to be the temporaryPointer which contains "NEW" firstPointer

    return i;
}

Item first(void){
    // check if empty first, if empty, there is no first element
    if (is_empty() == true){
        printf("Error: queue is empty\n");
        return 0;
    }
    return firstPointer->num; // if not empty, return the first element pointed by firstPointer
}

Item last(void){
    // check if empty first, if empty, there is no last element
    if (is_empty() == true){
        printf("Error: queue is empty\n");
        return 0;
    }
    return lastPointer->num; // if not empty, return the last element pointed by lastPointer
}

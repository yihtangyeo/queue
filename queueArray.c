/*
University of Toronto Faculty of Applied Science and Engineering
Division of Engineering Science
CSC192 Computer Programming and Data Structures

Title: Assignment 4
Question 2 (a): queueArray.c

Student name: YEO, Yih Tang
*/

#include <stdio.h>
#include <stdlib.h>

// define the size of queue to be 5
#define NUM 5
// define the maximum size of the array to be 100
#define MAX 100

typedef int Item;                   // define new data type Item that is int
typedef enum {false, true} bool;    // define new type of boolean that can have value true and false

// FUNCTION PROTOTYPES - DO NOT EDIT //
void make_empty(void);
bool is_empty(void);
bool is_full(void);
void enqueue (Item i);
Item dequeue(void);
Item first(void);
Item last(void);
//     END OF FUNCTION PROTOTYPE     //

Item queue[MAX];                    // create an array of size NUM that is the queue - NOTE: fized allocated memory

// create three variables storing the position of empty (last element), item to be removed (first element), and number of items currently stored in the array
int position_of_empty = 0, item_to_be_removed = 0, number_of_items = 0;

int main()
{

    int user_selection; // variable store the user selection based on the MENU displayed below
    int temporaryInt;   // variable temprarily stores the number that the user wants to put into the queue when enqueue is called

    printf("size of queue is declared to %d\n", NUM);

    do
    {
        // display the menu and prompt users to make a selection
        printf("Choose an action:\n1: make_empty()\n2: is_empty\n3: is_full\n4: enqueue\n5: dequeue\n6: first\n7: last\n8: QUIT\n");
        printf("Please choose: ");
        scanf("%d", &user_selection);

        // if user makes a selection beyond acceptable range 1-8, print error and show the menu again.
        if (user_selection < 1 || user_selection > 9){
            printf("Error: invalid selection.\n");
            continue;
        }

        switch(user_selection)
        {
            case 1:                         // make the list empty
                make_empty();
                break;
            case 2:                         // check if the list is empty
                printf("is_empty: ");
                if (is_empty())
                    printf("true\n");
                else
                    printf("false\n");
                break;
            case 3:                         // check if the list is full
                printf("is_full: ");
                if (is_full())
                    printf("true\n");
                else
                    printf("false\n");
                break;
            case 4:                         // enqueue an item to the END, prompt user to enter the value
                printf("Enter item to enqueue: ");
                scanf("%d", &temporaryInt);
                enqueue(temporaryInt);
                break;
            case 5:                         // dequeue an item from the BEGINNING
                printf("Item that is being dequeued: %d\n", dequeue());
                break;
            case 6:                         // access the first element in the queue
                printf("first: %d\n", first());
                break;
            case 7:                         // access the last element in the queue
                printf("last: %d\n", last());
                break;
        }

        printf("\n");

    } while (user_selection != 8);
    // repeat showing the menu until the user chooses to quit.

    return 0;
}

void make_empty(void){
    // as this is a fixed allocated memory, we can only set our counters, which are tehse variables below to zero
    position_of_empty = 0; item_to_be_removed = 0; number_of_items = 0;
    printf("Successful: Array is now empty\n");
}

bool is_empty(void){
    // check if empty = check if number_of_items stored in the queue is 0
    if (number_of_items == 0){
        return true;
    }
    else{
        return false;
    }
}

bool is_full(void){
    // if the queue is empty, it is impossible for it to be full
    if (is_empty() == true){
        return false;
    }
    // the queue is full when the number of items in the queue is the same as the NUM (size of queue, maximum)
    if (number_of_items == NUM){
        return true;
    }
    else{
        return false;
    }
}

void enqueue(Item i){
    // before inseting, check if the queue is full or not
    if (is_full() == true){
        printf("Fail to enqueue: Queue is full\n");
    }
    else{
        queue[position_of_empty] = i;               // add the value at the last element
        position_of_empty++;                        // increment as the last element is +1
        number_of_items++;                          // increment the total number of items in the queue
        printf("Enqueue successful: number %d is stored.\n", i);
    }
}

Item dequeue(void){
    // before dequeuing, check if there is something to be dequeued, which is, is the queue empty
    if (is_empty() == true){
        printf("Error: unable to dequeue, queue is empty\n");
        return 0;
    }
    Item i = queue[item_to_be_removed];             // remove the item from the beginning element of the queue
    item_to_be_removed++;                           // the "new" beginning is the next term, since the "old" beginning is "deleted"
    number_of_items--;                              // the number of items in the array
    return i;                                       // return the beginning element
}

Item first(void){
    // check if empty first, if empty, there is no first element
    if (is_empty() == true){
        printf("Error: queue is empty\n");
        return 0;
    }
    return queue[0];            // if not empty, return the first element
}

Item last(void){
    // check if empty first, if empty, there is no last element
    if (is_empty() == true){
        printf("Error: queue is empty\n");
        return 0;
    }
    return queue[position_of_empty-1]; // if not return the last element by accessing the elemen at index position_of_empty - 1, because the position_of_empty is always incremented by extra 1 when a new value is added
}
